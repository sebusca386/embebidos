import face_recognition as fr
import cv2
import RPi.GPIO as GPIO
import csv_collector
import numpy as np
import os
import random
from time import sleep

# Funcion para codificar los rostros
def codrostros(images):
    listacod = []
    # Iteramos
    for img in images:
        # Correccion de color
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # Codificamos la imagen
        cod = fr.face_encodings(img)[0]
        # Almacenamos
        listacod.append(cod)
    return listacod

# Funcion para led rojo relacionado con sensor de proximidad
def led(pin_led,pin_sensor): 
    # Configuracion del pin de led
    GPIO.setup(pin_led, GPIO.OUT)   # pin_led como salida
    GPIO.output(pin_led, GPIO.LOW)  # pin_led iniciado en estado bajo
    # Condiciones para que el led se encienda cuando el sensor detecte
    if GPIO.input(pin_sensor) != True:
        A=GPIO.output(pin_led, GPIO.HIGH)   # pin_led a estado alto
    else:
        A=GPIO.output(pin_led, GPIO.LOW)    # pin_led a estado bajo
    return A

if __name__=="__main__":

    # Accedemos ala carpeta
    path = 'Registrados'
    lista = os.listdir(path)
    images = []
    clases = []

    # Leemos los rostros del DB
    for lis in lista:
        # Leemos las imagenes de los rostros
        imgdb = cv2.imread(f'{path}/{lis}')
        # Almacenamos imagen
        images.append(imgdb)
        # Almacenamos nombre
        clases.append(os.path.splitext(lis)[0])
    print(clases)
    
    # Llamamos la funcion de codificacion de rostros para los rostros registrados
    rostroscod = codrostros(images)

    # Llamamos la funcion de registro en csv
    registrador=csv_collector.CSV()
    
    # Realizamos VidepCaptura
    cap = cv2.VideoCapture(0)
    cap.set(3,800)
    cap.set(4,600)

    # Iniciamos variables auxiliares
    min_aux = 0
    contador_frames = 0
    contador_puerta = 0
    comp1 = 100

    ## Definicion de entradas y salidas
    GPIO.setmode(GPIO.BCM)  # se utiliza los pines en formato BCM

    # Configuracion de sensor de proximidad
    pin_sensor = 17
    GPIO.setup (pin_sensor, GPIO.IN , pull_up_down = GPIO.PUD_UP) # pin_sensor como entrada
    
    # Configuracion de pulsador
    pin_pulsador = 22
    GPIO.setup (pin_pulsador, GPIO.IN , pull_up_down = GPIO.PUD_UP) # pin_pulsador como entrada

    # Configuracion de rele
    pin_rele=27
    GPIO.setup(pin_rele, GPIO.OUT) # pin_rele como salida
    GPIO.output(pin_rele, GPIO.LOW) # pin_rele iniciado en bajo

    # Configuracion de leds
    led_rojo= 23    # indicador de proximidad
    led_verde= 24   # indicador de autorizacion de entrada
    GPIO.setup(led_verde, GPIO.OUT) # led_verde como salida
    GPIO.output(led_verde, GPIO.LOW)  # led_verde inicializado en bajo


    try:
        while True:
            # Condiciones sensor de proximidad activo y puerta cerrada para iniciar captura
            if (GPIO.input(pin_sensor) == False) and (GPIO.input(pin_pulsador) == True):
                print ("Detectando")
                
                # Se inicia la captura de camara
                ret, frame = cap.read()

                # Reducimos las imagenes para mejor procesamiento
                frame2 = cv2.resize(frame, (0,0), None, 0.25, 0.25)

                # Conversion de color
                rgb = cv2.cvtColor(frame2, cv2.COLOR_BGR2RGB)

                # Buscamos los rostros
                faces = fr.face_locations(rgb)
                facescod = fr.face_encodings(rgb, faces)
                
                # Led indicador de porximidad
                fun_led=led(led_rojo,pin_sensor) 

                # Iteramos
                for facecod, faceloc in zip(facescod, faces):
                    
                    # Comparamos rostro de DB con rostro en tiempo real
                    comparacion = fr.compare_faces(rostroscod, facecod, 0.45)
                    
                    # Calculamos la similitud
                    simi = fr.face_distance(rostroscod, facecod)
                    
                    # Buscamos el valor mas bajo
                    min = np.argmin(simi)
                    
                    ## Si hay coincidencia con los rostros del registro, entonces:
                    if comparacion[min]:
                        # Se llama al rostro coincidente
                        nombre = clases[min]
                        print(nombre)
                        
                        # Se defina condicion para reconocer a solo una persona a la vez
                        # contando si la persona anterior es la misma, sino se reinicia el contador
                        if min == min_aux:
                            contador_frames = contador_frames+1
                            print(contador_frames)
                        else:
                            contador_frames = 0
                        min_aux = min
                        
                        #Si contador_frames llega a 30, se abrira la puerta
                        if contador_frames == 30:
                            min_aux = 0     #Se resetea 
                            # Se define la ruta
                            ruta="./Registrados/"+nombre+".jpeg"   
                            # Se registra la persona. 
                            # y indica que se abre la puerta en el registro
                            registrador.fill_csv(nombre, ruta,'Abierta')     
                            print("entrada permitida")
                            
                            # Accionamiento al autorizar entrada
                            GPIO.output(led_rojo, GPIO.LOW)     # led proximidad se apaga
                            GPIO.output(led_verde, GPIO.HIGH)   # led de autorizacion se enciende
                            GPIO.output(pin_rele, GPIO.HIGH)    # rele se activa por 1 segundo
                            sleep(1)
                            GPIO.output(pin_rele, GPIO.LOW)     # rele se cierra
                            
                            # Se dejan 5 segundo para simular que la puerta se abre
                            # con la desactivacion del pulsador
                            sleep(5)       
                            
                            # Mientras la puerta este abierta
                            # el proceso se pausa hasta que se vuelva a cerrar
                            while GPIO.input(pin_pulsador)==False:
                                print("Puerta abierta")
                                sleep(0.5)
                                
                            # Condicion cuando la puerta se vuelva a cerrar
                            if GPIO.input(pin_pulsador)==True:
                                contador_puerta=contador_puerta+1
                                if contador_puerta == 1:
                                    print("Cerrado")
                                    # Se registra que la puerta se cerro 
                                    registrador.fill_csv(nombre, ruta,'Cerrada')
                                    contador_puerta = 0
                                    # led de autorizacion se desactiva y se finaliza el proceso
                                    GPIO.output(led_verde, GPIO.LOW)
                                    break
                            

                        ## Proceso utilizado para visualizar la deteccion de los rostros
                        # Extraemos coordenadas
                        yi, xf, yf, xi = faceloc
                        # Escalamos
                        yi, xf, yf, xi = yi*4, xf*4, yf*4, xi*4

                        indice= comparacion.index(True)

                        # Comparamos
                        if comp1 != indice:
                            # Para dibujar cambiamos colores
                            r = random.randrange(0, 255, 50)
                            g = random.randrange(0, 255, 50)
                            b = random.randrange(0, 255, 50)
                            comp1 = indice

                        if comp1 == indice:
                            # Dibujamos
                            cv2.rectangle(frame, (xi, yi), (xf, yf), (r, g, b), 3)
                            cv2.rectangle(frame, (xi, yf-35), (xf, yf), (r, g, b), cv2.FILLED)
                            cv2.putText(frame, nombre, (xi+6, yf-6), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

                    ## Mientras no haya coincidencia del rostro con los registrados, no pasara nada    
                    else:
                        print("Desconocido")            


                # Mostramos Frames de la camara
                cv2.imshow("Reconocimiento Facial", frame)

                # Leemos el teclado, con tecla ESC se finliza la captura
                t = cv2.waitKey(5)
                if t == 27:
                    break

            ## Si el sensor de proximidad no detecta o la puerta esta cerrada, no se iniciara la captura
            else:
                print ("No se detecta proximidad o puerta esta abierta")
                sleep(2)
    ## Se puede finalizar el programa con la combinacion ctrl+c
    except KeyboardInterrupt:
        # Se limpian los pines GPIO
        GPIO.cleanup ()

