import cv2

# Ajuste de camara
camera = cv2.VideoCapture(0)    # Seleccion de entrada de camara
camera.set(3,800)               # Ajustes de resolucion de imagen 800x600
camera.set(4,600)

# Captura de imagen
while True:
    # Se inicia la camara
    ret , frame = camera.read()
    if ret is False:
        break
    # Se muestra lo que visualiza la camara
    cv2.imshow('Ventana',frame)
    # Se guarda la imagen (frame) con el nombre y direccion correspondiente
    cv2.imwrite('./Registrados/enrique_ramirez.jpeg',frame) 
    # Se detiene la camara con la tecla 27 (ESC) y se guarda el ultimo frame
    t = cv2.waitKey(1)
    if t == 27:
        break
camera.release()
cv2.destroyAllWindows()
