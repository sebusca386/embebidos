import os
import csv
import time
# import pandas

data = {'enrique_ramirez': [['Enrique', 'Ramírez'], '19.825.420-7', 'Estudiante de posgrado'],
        'daniel_mendez':   [['Daniel', 'Méndez'],   '20.098.016-6', 'Estudiante de pregrado'],
        'cristian_hott':   [['Cristian', 'Hott'],   '20.061.037-7', 'Estudiante de pregrado'],
        'matias_martinez': [['Matias', 'Martínez'], '19.540.757-6', 'Estudiante de pregrado']}

class CSV:
    def __init__(self):
        """ Definir archivo CSV con los nombres de las columnas y dirrección del archivo """

        # Variables de directorio y datos a muestrear
        self.recording_date = (time.ctime(time.time()).replace("  ", r" ").replace(":", r"-"))
        self.directory_csv = 'csv/' + self.recording_date + '.csv'
        self.fieldnames = ['Fecha', 'Hora', 'Puerta', 'Nombre', 'Apellido', 'RUT', 'Cargo', 'Ruta imagen rostro']
        self.count = 0

        # Si la ruta de la carpeta no existe, se crea
        if not os.path.exists('csv/'):
            os.makedirs('csv/')
            print('Directorio creado:', 'csv/')

        # Crear archivo csv
        with open(self.directory_csv, mode='w', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)
            csv_file.writeheader()

    def fill_csv(self, name, face_route, door):
        """ Recopilación datos del registro de rostros, se rellena una fila del .csv con la información de cada rostro 
            ingresado en el sistema """
            
        self.count += 1
        with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

            # Añadir fila al archivo .csv
            row = {'Fecha': time.strftime('%Y-%m-%d', time.localtime()),
                   'Hora': time.strftime('%H:%M:%S', time.localtime()),
                   'Puerta': door,
                   'Nombre': data[name][0][0],
                   'Apellido': data[name][0][1],
                   'RUT': data[name][1],
                   'Cargo': data[name][2],
                   'Ruta imagen rostro': face_route}

            csv_file.writerow(row)

    # def show_csv(self):
    # Mostrar los datos del csv en consola
    # print(pandas.read_csv(self.directory_csv))





