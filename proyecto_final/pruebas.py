import face_recognition
import cv2
import os
from picamera import PiCamera
from io import BytesIO
from time import sleep
import RPi.GPIO as GPIO
if __name__=="__main__":
    GPIO.setmode(GPIO.BOARD)
    #GPIO.setup(17, GPIO.OUT)#sensor
    GPIO.setup(7, GPIO.OUT)#rele#es el pin donde se ubica

    GPIO_PIN = 11
    GPIO.setup (GPIO_PIN, GPIO.IN , pull_up_down = GPIO.PUD_UP) 
    #GPIO.output(17, False)
    GPIO.output(7, False)
    # Create an in-memory stream
    img = BytesIO()
    camera = PiCamera()
    camera.start_preview()
    # Camera warm-up time
    sleep(2)
    camera.capture(img, 'jpeg')
    print("pass")
    GPIO.output(7, True)

 
    # Pause between output is defined (in seconds) 
    delayTime  =  0.5
 
    print ("Sensor test [push CTRL+C to stop the test]") 
 
    # Main program loop
    try:
        while True:
            if GPIO.input(GPIO_PIN) == True:
                print ("No obstacle") 
            else:
                print ("obstacle detected") 
            print ("---------------------------------------")
    
            # Reset + Delay
            sleep (delayTime) 
    
    # Tidying up after the program has been terminated
    except KeyboardInterrupt:
        GPIO.cleanup ()
        #GPIO.cleanup()