# Credit: Adrian Rosebrock
# https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/

# import the necessary packages
from picamera.array import PiRGBArray # Generates a 3D RGB array
from picamera import PiCamera # Provides a Python interface for the RPi Camera Module
import time # Provides time-related functions
import cv2 # OpenCV library

# Initialize the camera
#camera = PiCamera()
#Camara USB
camera = cv2.VideoCapture(0)
camera.set(3,800)
camera.set(4,600)

# Set the camera resolution
#camera.resolution = (640, 480)

# Set the number of frames per second
#camera.framerate = 32

# Generates a 3D RGB array and stores it in rawCapture
#raw_capture = PiRGBArray(camera, size=(640, 480))

# Wait a certain number of seconds to allow the camera time to warmup
#time.sleep(0.1)



while True:
    ret , frame = camera.read()
    if ret is False:
        break
    #cv2.imwrite('./unknown/sujeto.jpeg',frame)
    cv2.imshow('Ventana',frame)
    t = cv2.waitKey(1)
    if t == 27:
        break
camera.release()
cv2.destroyAllWindows()