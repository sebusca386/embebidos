// Proyecto Unidad 1


#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 5

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);


// Pines digitales
const int rojo = 4;
const int amarillo = 3;
const int verde = 2;
const int relay = 10;

// Valor sensor de temperatura
float temperatura;
float suma_temp;
float temp_promedio;
int tiempo_rojo = 0;

// Umbrales temperatura
const float temp0 = 24;

void setup() {
  // Inicializar pines digitales
  pinMode(rojo, OUTPUT);
  pinMode(amarillo, OUTPUT);
  pinMode(verde, OUTPUT);
  pinMode(relay, OUTPUT);

  Serial.begin(9600);
}

void loop() {

  sensors.requestTemperatures(); // Send the command to get temperatures
  // We use the function ByIndex, and as an example get the temperature from the first sensor only.
  float temperatura = sensors.getTempCByIndex(0);
  
  suma_temp = 0;
  for (int i=0; i < 5; i++){
    suma_temp = temperatura + suma_temp;
    delay(100);
  }

  //Serial.println(suma_temp/5.0, 1);
  temp_promedio=suma_temp/5.0;
  Serial.println(temp_promedio);
  
  if (temp_promedio <= temp0){
    digitalWrite(verde, HIGH);
    digitalWrite(amarillo, LOW);
    digitalWrite(rojo, LOW);
    digitalWrite(relay, HIGH);

    tiempo_rojo = 0;
  }

  else if (temp_promedio > temp0  &&  temp_promedio <= (temp0 + 5.0)){
    digitalWrite(verde, LOW);
    digitalWrite(amarillo, HIGH);
    digitalWrite(rojo, LOW);
    digitalWrite(relay, HIGH);

    tiempo_rojo = 0;
  }

  else if (temp_promedio > (temp0 + 5.0) && tiempo_rojo < 6){
    digitalWrite(verde, LOW);
    digitalWrite(amarillo, LOW);
    digitalWrite(rojo, HIGH);
    digitalWrite(relay, HIGH);

    tiempo_rojo += 1;
    Serial.println(tiempo_rojo);
  }

  else if (tiempo_rojo >=6) {
    digitalWrite(verde, LOW);
    digitalWrite(amarillo, LOW);
    digitalWrite(rojo, LOW);
    digitalWrite(relay, LOW);
  }  

}
