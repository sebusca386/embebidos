#Archios importados para trabajar
from csv_collector import CSV
from lector_sensor import SensorPeso

#Librerias para dash,gráficos, analisis de datos, entre otros
import plotly.express as px
import pandas as pd
from dash import Dash, dcc, html
import dash_daq as daq
import os,glob
from pynput import keyboard as kb


#Inicio de Dash
app= Dash(__name__)

#Se define los pines en los que estan conectados los sensores en el orden: Dt , Sck
agua=SensorPeso(11, 10, "AGUA")
comida=SensorPeso(21, 20, "COMIDA")
#Inicialización de los sensores
agua.inicio_sensor()
comida.inicio_sensor()
#Calibración predefinida de los sensores
agua.get_config_sensor()
comida.get_config_sensor()
#Inicio del archivo csv
file_csv = CSV()
#CDefinición de colores del dashboard
colors = {'background': 'rgb(23, 27, 29)','text': '#7FDBFF'}     


#Definimos las funciones que se asociarán al escuchador
#las funciones deben invocar a press y release
def pulsa(teclas):
	hotkey.press(escuchador.canonical(teclas))
def suelta(teclas):
	hotkey.release(escuchador.canonical(teclas))

#definimos la función que se ejecutará al detectar la combinación
def pulsa_ctrl_t():
	app.run_server(debug=False, host='0.0.0.0') #Servidor local 127.0.0.1:8050

#definimos la combinación de teclas que queremos detectar
ctrl_t = kb.HotKey.parse('<ctrl>+t')

#indicamos qué función se ejecutará al detectarla
hotkey = kb.HotKey(ctrl_t, pulsa_ctrl_t)  #Al presionar "ctrl+t" arranca el servidor dashboard

#creamos y lanzamos el escuchador
escuchador = kb.Listener(pulsa, suelta)
escuchador.start()

try:
    while True:   
        #Obtención de los valores de los sensores 
        V_comi = comida.get_value()
        V_agua = agua.get_value()
        #Se almacenan en el archivo csv, la posición 0 es comida y 1 agua
        file_csv.fill_csv([0, 1], [V_comi, V_agua])

        #Leer el ultimo csv creado
        ruta="csv"  #Ruta de la carpeta que contendrá los archivos csv
        archivos=glob.glob(ruta+os.path.sep+"*")
        #Ordena según fecha de creación los archivos existentes en la carpeta csv. 
        #El primer archivo de la lista será el ultimo creado
        archivos.sort(key= os.path.getctime,reverse=True)   
        if len(archivos)<1: #Si la carpeta no contiene archivos
            ult_csv=[]      #se dejara vacia la lista
        else:
            ult_csv=archivos[0]     #Sino, se leerá el primer archivo de la lista

        #Lectura de datos
        if ult_csv!=[]:     #si es distinto de vacio, entonces
            #Se lee el último archivo creado, separando las columnas por ","
            df = pd.read_csv(ult_csv, delimiter = ',')  
            #Se leen los últimos 50 datos almacenado del csv (asignar valor a preferencia)
            df_ult_fil=df.tail(50)  
        
        #Gráfico de lineas para pesos 
        #Se grafican los ultimos 50 valores, como el arvicho se abre continueamente, 
        #esto hace un efecto de desplzamiento en la gráfica
        fig=px.line(df_ult_fil, x="Hora", y="Peso leido",color="ID deposito",symbol="ID deposito")      
        #Se cambia colores de la gráfica
        fig.update_layout(plot_bgcolor=colors['background'],
        paper_bgcolor=colors['background'],font_color=colors['text'])
        
        
        #Se inicia el Layout
        app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
        #Cuadro que contiene título
        html.H1(children='Panel de monitoreo',
        style={'textAlign': 'center','color': colors['text'],}),
        #Cuadro con pequeña descripción
        html.Div(children='Dash para monitoreo de comedero', 
        style={'textAlign': 'center','color': colors['text'],}),
        
        #Se presenta el gráfico de lineas en primer orden
        html.Div([
        dcc.Graph(id='graph-barra',figure=fig), #Se llama al gráfico creado
        dcc.Interval(
            id='interval-component',
            interval=1*1000, # in milliseconds
            n_intervals=0,
        )
        ]),
        #Cuadro que tendra los displays y barras de estado
        html.Div([
            html.Div([
                #Display de gramos de comida
                html.Div(
                    daq.LEDDisplay(id='display-1',
                    label="Comida", value=round(V_comi,2), size=40, 
                    backgroundColor=colors['background'],
                    style={'textAlign': 'center','color': colors['text'],
                    'margin-top':'0px','margin-left':'0px',},
                )),
                #Display de gramos de agua
                html.Div(
                    daq.LEDDisplay(id='display-2',
                    label="Agua", value=round(V_agua,2), size=40, 
                    backgroundColor=colors['background'],
                    style={'textAlign': 'center','color': colors['text'],
                    'margin-top':'0px','margin-left':'0px',}
                )),
            ],style={'display':'flex','justify-content': 'space-around'}), 
            #Se hace una alineación de los displays para ubicarlos


            html.Div([
                #Barra de estado de porcentaje de comida
                html.Div(
                        daq.GraduatedBar(id='batery-1',
                        color={"gradient":True,"ranges":{"green":[0,4],
                        "yellow":[4,7],"red":[7,10]}},
                        showCurrentValue=True, value=round((100*V_comi)/1000,1)/10, size=600,
                        )
                ),
                #Barra de estado de porcentaje de agua
                html.Div(
                        daq.GraduatedBar(id='batery-2',
                        color={"gradient":True,"ranges":{"green":[0,4],
                        "yellow":[4,7],"red":[7,10]}},
                        showCurrentValue=True, value=round((100*V_agua)/1000,1)/10, size=600,
                        )
                ),
            ],style={'display':'flex','justify-content': 'space-around','margin-top': '20px'}),
            #Se hace una alineación de las barras de estado para ubicarlos
             ])
        ])

#Se interrumpe el programa con "ctrl+c"   
except(KeyboardInterrupt,SystemExit):
    print("Finalización")