"""Metodos para leer el sensor.

Este modulo tiene las funciones para calibrar y leer el sensor HX711.
Esta codificado para la placa de prueba raspberry pi.

Example:
    Ejemplo de calibracion del sensor. 
    Antes de generar lecturas continas al sensor se recomiendo primero calibrarlo, 
    para calibrar el sensor ejecutar la siguiente linea en el terminal::

        $ python3 lector_sensor.py

Los datos obtenidos para la calibracion se guardar en un archivo config.ini.
"""
import RPi.GPIO as GPIO 
from hx711 import HX711
import configparser
import os
GPIO.setwarnings(False)

"""Modulos necesarios para trabajar con el metodo
Rpi: Modulo por defecto en la raspberry pi.
hx711: Modulo especifico del sensor (instalar requerimientos antes de utilizar).
"""

class SensorPeso:
    """Clase que se encarga para realizar lectura de peso al sensor HX711.

    Este módulo es una interface entre las celdas de carga y el microcontrolador, 
    permitiendo poder leer el peso de manera sencilla. 
    Internamente se encarga de la lectura del puente wheatstone formado por la celda de carga, 
    convirtiendo la lectura analógica a digital con su conversor A/D interno de 24 bits.
    """
    def __init__(self, dout, sck, name):
        """Contruccion del sistema apara leer objeto.

        "__init__" es un método reservado en las clases de Python.
        Es conocido como un constructor en conceptos orientados a objetos. 
        Este método se llama cuando se crea un objeto a partir de la clase 
        y permite que la clase inicialice los atributos de una clase.

        Note:
            Do not include the `self` parameter in the ``Args`` section.

        Args:
            dout (int): Pin para obtener los datos den sensor.
            sck (int): Pin paa setiar la señal del reloj.
        """
        self.name = name
        self.dout = dout
        self.sck = sck
        self.status = False
    def inicio_sensor(self):
        try:
            GPIO.setmode(GPIO.BCM)  # Pines GPIO en numeración BCM
            # Crea un objeto hx que represente el chip HX711 real
            # Los parámetros de entrada obligatorios son solo 'Pin_Dato' y 'PD_sck'
            self.hx = HX711(dout_pin=self.dout, pd_sck_pin=self.sck)
            # Medir la tara y guardar el valor como compensación para el canal actual
            # y ganancia seleccionada. Eso significa canal A y ganancia 128
            self.err = self.hx.zero()
            # Verifica si todo está correcto
            if self.err:
                raise ValueError('La tara no se puede definir.')

            self.reading = self.hx.get_raw_data_mean()
            if self.reading:     # Verificar si el valor correcto 
                            # ahora el valor está cerca de 0
                print('Datos restados por compensación pero todavía no convertidos a unidades:', self.reading)
            else:
                print('Dato invalido', self.reading)
        except (KeyboardInterrupt, SystemExit):
            print('Falla en el sistema')
            self.shutdown()
    def get_config_sensor(self):
        if os.path.isfile('config.ini'):
            self.config = configparser.ConfigParser()
            self.config.read('config.ini')
            if self.name in self.config:
                self.hx.set_scale_ratio(float(self.config[self.name]['ratio']))
                self.status = True
            else:
                self.status = False
        else:
            self.status = False
    def calibracion_sensor(self):

        input('Coloque un peso conocido en la balanza y luego presione Enter')
        reading = self.hx.get_data_mean()
        if reading:
            print('Valor medio de HX711 restado para compensar:', reading)
            known_weight_grams = input(
                'Escriba cuántos gramos eran y presiona Enter: ')
            try:
                value = float(known_weight_grams)
                print(value, 'gramos')
            except ValueError:
                print('Entero o flotante esperado y tengo:',
                    known_weight_grams)
                self.shutdown()
                
            # establecer la relación de escala para un canal en particular y una ganancia
            # utilizada para calcular la conversión a unidades. El argumento requerido es solo
            # una relación de escala. Sin argumentos 'canal' y 'ganancia_A' establece
            # la relación entre el canal actual y la ganancia.
            ratio = reading / value   # calcular la relación para el canal A y la ganancia 128
            self.hx.set_scale_ratio(ratio) # Determina la proporción para el canal actual
            print('Relación de peso establecida.') 
            if os.path.isfile('config.ini'):
                self.config = configparser.ConfigParser()
                self.config.sections()
                self.config.read('example.ini')
                self.config.sections()
                self.config[self.name] = {}
                self.config[self.name]['ratio'] = str(ratio)
                self.config[self.name]['value'] = str(value)
                self.config[self.name]['reading'] = str(reading)
                with open('config.ini', 'a+') as configfile:
                    self.config.write(configfile)
            else:
            #print(type(self.name))
            #self.config['DEFAULT']['ratio'] = str(ratio)
            #self.config['agua']['ratio'] = str(ratio)
                self.config = configparser.ConfigParser()
                self.config[self.name] = {}
                self.config[self.name]['ratio'] = str(ratio)
                self.config[self.name]['value'] = str(value)
                self.config[self.name]['reading'] = str(reading)
                with open('config.ini', 'a+') as configfile:
                    self.config.write(configfile)
            self.status = True
        else:
            raise ValueError('No se puede calcular el valor medio . ERROR', reading)
            self.shutdown()
    def get_value(self):
        if self.status:
            return self.hx.get_weight_mean(20)
        else:
            return None
    def shutdown(self):
        GPIO.cleanup()
if __name__=="__main__":
    """Ejemplo de ejecucion del programa.

    El programa inicio un objeto con las configuraciones basicas
    luego inicia el sensor para obtener datos, por defecto se asume que no se ha
    calibrado por este motivo inicia la calibracion automatica, en caso contrario remplazar
    la funcion calibracion_sensor por get_config_sensor.
    """
    name_sensor = input("Nombre del elemento a medir (COMIDA o AGUA): ")
    s1 = SensorPeso(11, 10, name_sensor)
    s1.inicio_sensor()
    s1.calibracion_sensor()
    print(s1.get_value())
