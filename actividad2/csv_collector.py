import os
import csv
import time


# import pandas

class CSV:
    def __init__(self):
        """ Definir archivo CSV con los nombres de las columnas y dirrección del archivo """

        # Variables de tiempo, directorio y datos a muestrear
        self.current_date = time.time()
        self.recording_date = (time.ctime(self.current_date).replace("  ", r" ").replace(":", r"-"))
        self.directory_csv = 'csv/' + self.recording_date + '.csv'
        self.fieldnames = ['ID deposito', 'Fecha', 'Hora', 'Correlativo lectura', 'Peso leido']
        self.deposits_weight_samples = {}
        self.count = 0
        self.trs_v = ["Comida" ,"Agua"]

        # Si la ruta de la carpeta no existe, se crea
        if not os.path.exists('csv/'):
            os.makedirs('csv/')
            print('Directorio creado:', 'csv/')

        # Crear archivo csv
        with open(self.directory_csv, mode='w', encoding='UTF8', newline='') as file:
            csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)
            csv_file.writeheader()

    def fill_csv(self, deposits_id, weight):
        """ Recolección datos del muestreo y rellenar CSV cada 5 segundos con los datos obtenidos """

        # Recolección de datos por deposito
        for i in range(len(deposits_id)):
            if not deposits_id[i] in self.deposits_weight_samples:
                try:
                    self.deposits_weight_samples[deposits_id[i]] = [float(weight[i])]
                except ValueError:
                    pass
            else:
                try:
                    self.deposits_weight_samples[deposits_id[i]].append(float(weight[i]))
                except ValueError:
                    pass

        # Guardar los datos en el .csv cada 5 segundos
        if time.time() - self.current_date >= 4.0:
            self.count += 1
            with open(self.directory_csv, mode='a', encoding='UTF8', newline='') as file:
                csv_file = csv.DictWriter(file, fieldnames=self.fieldnames)

                for d in self.deposits_weight_samples.keys():
                    # Promedio valores de la muestra del deposito
                    try:
                        average_weight = round(sum(self.deposits_weight_samples[d]) / len(self.deposits_weight_samples[d]), 2)

                    except ZeroDivisionError:
                        average_weight = ''

                    # Añadir fila al archivo .csv
                    row = {'ID deposito': self.trs_v[d],
                           'Fecha': time.strftime('%Y-%m-%d', time.localtime()),
                           'Hora': time.strftime('%H:%M:%S', time.localtime()),
                           'Correlativo lectura': self.count,
                           'Peso leido': average_weight}

                    csv_file.writerow(row)

                    # Restablecer la variable de muestreo del deposito
                    self.deposits_weight_samples[d] = []

                # Actualizar variable auxiliar de tiempo
                self.current_date = time.time()

    # def show_csv(self):
    # Mostrar los datos del csv en consola
    # print(pandas.read_csv(self.directory_csv))